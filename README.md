## PHP - Task # 1 (required)
Please write a simple application that manages a contact list.
Functionality:
<<<<<<< HEAD
=======

1. Login
2. View contact list (available to every user) The list should contain basic data. After selecting a particular contact, display his details.
3. Edit and delete existing entries and add new ones (available only to a logged in user)

A single contact should have at least:

1. name,
2. surname,
3. email - unique,
4. phone number,
5. date of birth.

Technical assumptions:

1. The application should be written minimum with PHP5.5 using the MySQL 5 database
2. We suggest that validation of forms takes place without reloading the page.
3. The use of Symfony 3 components (Symfony 2 allowed) or Laravel is preferred but not required
4. Other free libraries may be used.
5. Pay attention to application security.
6. Appearance of the application is not really important.

Files, ERD schema and database dump should be sent by e-mail in a zipped archive
(7-zip or ZIP) to i.zhariy@npc.pl
---
## SQL / ERD - Task 2 (additional)
Design the database described below.

1. Draw an ERD diagram, mark the keys and foreign keys.
2. Next show the content of SQL queries returning the requested data below.
3. Point the columns, which you need to index to speed up your queries.
4. Use only SQL.

Database description:
The database contains information about persons (born after 1900) such as: name, surname, date of birth,
gender, earnings. Anyone can have a mother and / or father. A woman may have one husband, a man may
have one wife. People work in companies with names. One person can work in several companies
simultaneously, as an employee or subcontractor. Each company has exactly one CEO.
Requested data:

1. Find the name of the person with the greatest number of grandchildren.
3. Find the average number of employees and the average number of subcontractors in all companies.
3. Find a family (max two generations) with the least earnings. List the name of any person in this family.

Hint: A 1st generation family is an X person with a possible spouse. A 2nd generation family is a one generatio
family with all its children (including possible spouses) or parents.